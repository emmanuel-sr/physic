package main.java.simulator;

import main.java.simulator.model.Body;
import main.java.simulator.model.Circle;
import main.java.simulator.utils.CircleCircleCollision;
import main.java.simulator.model.Vector;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

import static main.java.simulator.utils.Key.GRAVITY;
import static main.java.simulator.utils.Key.RESTART;

public class World {

    private static final double GRAVITY_ACCELERATION = -9.81;

    private boolean gravitation = false;

    private List<Body> bodies;

    public World() {
        bodies = new ArrayList<>();
        init();
    }

    public void init() {
        bodies.clear();
        Circle circle1 = new Circle("Magenta Circle");
        circle1.setMass(1.5);
        circle1.setCenterPt(new Vector(100, SimulatorPanel.getHeightScaled() / 2));
        circle1.setVelocity(new Vector(50, 0));
        Circle circle2 = new Circle("Orange Circle");
        circle2.setCenterPt(new Vector(500, SimulatorPanel.getHeightScaled() / 2));
        circle2.setVelocity(new Vector(-50, 0));
        circle2.setFillColor(Color.ORANGE);
        bodies.add(circle1);
        bodies.add(circle2);
    }

    public List<Body> getBodies() {
        return bodies;
    }

    
    public void render(java.awt.Graphics2D g) {
        int windowHeight = SimulatorPanel.getHeightScaled();
        int textXPos = 0;
        g.setColor(Color.GREEN);
        g.fillRect(0, 0, SimulatorPanel.getWidthScaled(), windowHeight);
        g.setColor(Color.black);
        g.setFont(new Font("Consolas", Font.PLAIN, 12));
        for (Body body : bodies) {
            body.drawFill(g, windowHeight);
            body.drawOutline(g, windowHeight);
        }

        bodies.get(0).drawProperties(g,0,0);
        bodies.get(1).drawProperties(g,SimulatorPanel.getWidthScaled()/2,0);

    }

    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == GRAVITY.getKeyCode()) {
            gravitation = !gravitation;
        }
        if (e.getKeyCode() == RESTART.getKeyCode()) {
            init();
        }
    }

    public void update(double deltaTime) {
        for (Body body : bodies) {
            body.getNetForce().setX(0);
            body.getNetForce().setY(0);
        }
        addForces();
        translateBodies(deltaTime);
        computeCollision();
    }

    private void addForces() {
        // Adding gravitational force
        if (gravitation) {
            for (Body body : bodies) {
                if (!body.isMoving()) continue;
                body.getNetForce().setY(body.getNetForce().getY() + GRAVITY_ACCELERATION);
            }
        }
    }

    private void translateBodies(double deltaTime) {

        for (Body body : bodies) {
            if (!body.isMoving()) continue;

            Vector acceleration = new Vector(body.getNetForce().getX() / body.getMass(),
                    body.getNetForce().getY() / body.getMass());

            body.getVelocity().addTo(acceleration.multiply(deltaTime));

            body.translate(body.getVelocity().multiply(deltaTime));
        }
    }

    private void computeCollision() {
        for (int i = 0; i < bodies.size(); i++) {
            for (int j = i + 1; j < bodies.size(); j++) {
                Circle firstBody = (Circle) bodies.get(i);
                Circle secondBody = (Circle) bodies.get(j);
                Vector mtd = new Vector(0, 0);
                Vector circle1TransVector = new Vector(0, 0);
                Vector circle2TransVector = new Vector(0, 0);
                if (CircleCircleCollision.doBodiesCollide(firstBody, secondBody, circle1TransVector, circle2TransVector, mtd)) {
                    // System.out.println("Circles collided!");
                    firstBody.translate(circle1TransVector);
                    secondBody.translate(circle2TransVector);
                    calculateImpulse(firstBody, secondBody, mtd);
                }
            }
        }
    }

    private void calculateImpulse(Body body1, Body body2, Vector mtd) {
        double body1InverseMass = 1 / body1.getMass();
        double body2InverseMass = 1 / body2.getMass();

        if (!body1.isMoving())
            body1InverseMass = 0;

        if (!body2.isMoving())
            body2InverseMass = 0;

        // Calculate relative velocity
        Vector relativeVelocity = body2.getVelocity().subtract(body1.getVelocity());
        Vector normal = mtd.clone();
        normal.normalise();

        // Calculate relative velocity in terms of the normal direction
        double velAlongNormal = normal.dotProduct(relativeVelocity);

        // Calculate restitution
        double coefficientOfRestitution = Math.min(body1.getCoefficientOfRestitution(), body2.getCoefficientOfRestitution());

        // Calculate impulse scalar
        double totalImpulse = -(1.0f + coefficientOfRestitution) * velAlongNormal;
        totalImpulse /= body1InverseMass + body2InverseMass;

        // Apply impulse to each object
        Vector impulse = normal.multiply(totalImpulse);
        body1.setVelocity(body1.getVelocity().subtract(impulse.multiply(body1InverseMass)));
        body2.setVelocity(body2.getVelocity().add(impulse.multiply(body2InverseMass)));
    }

}
