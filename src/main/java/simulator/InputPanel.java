package main.java.simulator;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import main.java.simulator.model.Body;

public class InputPanel extends JPanel {

    List<Body> bodies;

    JLabel lbVelocityOne;
    JTextField txtVelocityOne;

    JLabel lbVelocityTwo;
    JTextField txtVelocityTwo;

    JLabel lbMassOne;
    JTextField txtMassOne;

    JLabel lbMassTwo;
    JTextField txtMassTwo;

    JButton accept;

    public InputPanel(List<Body> bodies) {
        this.bodies = bodies;
        init();
    }

    private void init() {
        lbVelocityOne = new JLabel("Velocity 1:");
        txtVelocityOne = new JTextField(10);
        lbVelocityOne.setLabelFor(txtVelocityOne);

        lbVelocityTwo = new JLabel("Velocity 2:");
        txtVelocityTwo = new JTextField(10);
        lbVelocityTwo.setLabelFor(txtVelocityTwo);

        lbMassOne = new JLabel("Mass 1:");
        txtMassOne = new JTextField(10);
        lbMassTwo = new JLabel("Mass 2:");
        txtMassTwo = new JTextField(10);

        accept = new JButton("accept");

        accept.addActionListener((e) -> {
            String mass1 = txtMassOne.getText();
            String mass2 = txtMassTwo.getText();
            String velocity1 = txtVelocityOne.getText();
            String velocity2 = txtVelocityTwo.getText();

            if (isNumeric(mass1)) {
                bodies.get(0).setMass(Integer.parseInt(mass1));
            }

            if (isNumeric(mass2)) {
                bodies.get(1).setMass(Integer.parseInt(mass2));
            }

            if (isNumeric(velocity1)) {
                bodies.get(0).getVelocity().setLength(Double.parseDouble(velocity1));
            }

            if (isNumeric(velocity2)) {
                bodies.get(1).getVelocity().setLength(Double.parseDouble(velocity2));
            }
            cleanInputText();
        });

        add(lbVelocityOne);
        add(txtVelocityOne);
        add(lbVelocityTwo);
        add(txtVelocityTwo);
        add(lbMassOne);
        add(txtMassOne);
        add(lbMassTwo);
        add(txtMassTwo);
        add(accept);
    }

    private void cleanInputText() {
        txtMassOne.setText("");
        txtMassTwo.setText("");
        txtVelocityOne.setText("");
        txtVelocityTwo.setText("");
    }

    public static boolean isNumeric(String strNum) {
        return strNum.matches("-?\\d+(\\.\\d+)?");
    }
}
