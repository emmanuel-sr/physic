package main.java.simulator.model;

public class Vector {

    private double x;
    private double y;

    public Vector(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public void setAngle(double angle) {
        double length = this.getLength();
        this.x = Math.cos(angle) * length;
        this.y = Math.sin(angle) * length;
    }

    public double getAngle() {
        return Math.atan2(this.y, this.x);
    }

    public void setLength(double length) {
        double angle = this.getAngle();
        this.x = Math.cos(angle) * length;
        this.y = Math.sin(angle) * length;
    }

    public double getLength() {
        return Math.sqrt(this.x * this.x + this.y * this.y);
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public Vector add(Vector vector) {
        return new Vector(this.x + vector.getX(), this.y + vector.getY());
    }

    public Vector subtract(Vector vector) {
        return new Vector(this.x - vector.getX(), this.y - vector.getY());
    }

    public Vector multiply(double scalar) {
        return new Vector(this.getX() * scalar, this.getY() * scalar);
    }

    public double distance(Vector vector) {
        return Math.sqrt(this.distanceSquared(vector));
    }

    public double distanceSquared(Vector vector) {
        return Math.pow(this.getX() - vector.getX(), 2) + Math.pow(this.getY() - vector.getY(), 2);
    }

    public double dotProduct(Vector vector) {
        return (this.getX() * vector.getX()) + (this.getY() * vector.getY());
    }

    public void addTo(Vector vector) {
        this.x += vector.getX();
        this.y += vector.getY();
    }

    public void subtractFrom(Vector vector) {
        this.x -= vector.getX();
        this.y -= vector.getY();
    }

    public void multiplyBy(double scalar) {
        this.x *= scalar;
        this.y *= scalar;
    }

    public void divideBy(double scalar) {
        this.x /= scalar;
        this.y /= scalar;
    }

    public void normalise() {
        this.divideBy(getLength());
    }

    @Override
    public Vector clone() {
        return new Vector(this.x, this.y);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Vector that = (Vector) obj;
        return (x == that.x) && (y == that.y);
    }

    @Override
    public String toString() {
        return "Vector[" + x + ", " + y + "]";
    }

    public String rounded() {
        return "Vector[" + Math.round(x * 100) / 100.0 + ", " + Math.round(y * 100) / 100.0 + "]";
    }

}
