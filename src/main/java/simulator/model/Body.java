package main.java.simulator.model;

import java.awt.*;

public abstract class Body {

    private Vector centerPt = new Vector(0, 0);
    private Vector netForce = new Vector(0, 0);
    private Vector velocity = new Vector(0, 0);

    private Color outlineColor = Color.BLUE;
    private Color fillColor = Color.MAGENTA;

    private String name;

    private double mass = 1;
    private double angle = 0;
    private double coefficientOfRestitution = 0.8;

    private boolean isMoving = true;

    public Body(String bodyName) {
        this.setName(bodyName);
    }

    public abstract void translate(Vector displacement);

    public double getMass() {
        return mass;
    }

    public void setMass(double newMass) {
        this.mass = newMass;
    }

    public double getCoefficientOfRestitution() {
        return coefficientOfRestitution;
    }

    public String getName() {
        return name;
    }

    public void setName(String newName) {
        this.name = newName;
    }

    public Vector getCenterPt() {
        return centerPt;
    }

    public void setCenterPt(Vector newCenterPt) {
        this.centerPt = newCenterPt;
    }

    public Vector getNetForce() {
        return netForce;
    }

    public void setNetForce(Vector newNetForce) {
        this.netForce = newNetForce;
    }

    public Vector getVelocity() {
        return velocity;
    }

    public void setVelocity(Vector newVelocity) {
        this.velocity = newVelocity;
    }

    public double getAngle() {
        return angle;
    }

    public void setAngle(double angle) {
        this.angle = angle;
    }

    public boolean isMoving() {
        return isMoving;
    }

    public void setMoveable(boolean isMoving) {
        this.isMoving = isMoving;
    }

    public Color getFillColor() {
        return fillColor;
    }

    public void setFillColor(Color color) {
        this.fillColor = color;
    }

    public Color getOutlineColor() {
        return outlineColor;
    }

    public void setOutlineColor(Color outlineColor) {
        this.outlineColor = outlineColor;
    }

    public void drawOutline(Graphics g, int referenceHeight) {
        g.drawOval((int) getCenterPt().getX() - 2, referenceHeight - (int) getCenterPt().getY() - 2, 4, 4);
    }

    public void drawFill(Graphics g, int referenceHeight) {
        g.fillOval((int) getCenterPt().getX() - 2, referenceHeight - (int) getCenterPt().getY() - 2, 4, 4);
    }

    public void drawBoundingBox(Graphics g, int referenceHeight) {
        g.drawRect((int) getCenterPt().getX() - 2, referenceHeight - (int) getCenterPt().getY() - 2, 4, 4);
    }

    public void drawProperties(Graphics g, int x, int y ) {
        for (String line : this.toString().split("\n"))
            g.drawString(line, x, y += g.getFontMetrics().getHeight());
    }

    @Override
    public String toString() {
        return name + " {\n" +
                "   mass : " + mass + ",\n" +
                "   position : " + centerPt.rounded() + ",\n" +
                "   velocity : " + velocity.rounded() + ",\n" +
                "   netForce : " + netForce.rounded() + "\n}";
    }

}
