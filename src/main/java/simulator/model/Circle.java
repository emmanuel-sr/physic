package main.java.simulator.model;

import java.awt.*;

public class Circle extends Body {

    private double radius = 20;

    public Circle(String name) {
        super(name);
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double newRadius) {
        radius = newRadius;
    }

    public void translate(Vector displacement) {
        getCenterPt().addTo(displacement);
    }

    @Override
    public void drawFill(Graphics g, int referenceHeight) {
        Rectangle boundingBox = getTranslatedBoundingBox(referenceHeight);
        g.setColor(getFillColor());
        g.fillOval(boundingBox.x, boundingBox.y, boundingBox.width, boundingBox.height);

        // Draw the center of mass
        super.drawFill(g, referenceHeight);
    }

    @Override
    public void drawOutline(Graphics g, int referenceHeight) {
        Rectangle boundingBox = getTranslatedBoundingBox(referenceHeight);
        g.setColor(getOutlineColor());
        g.drawOval(boundingBox.x, boundingBox.y, boundingBox.width, boundingBox.height);

        // Draw the center of mass
        super.drawOutline(g, referenceHeight);
    }

    @Override
    public void drawBoundingBox(Graphics g, int referenceHeight) {
        Rectangle boundingBox = getTranslatedBoundingBox(referenceHeight);
        g.setColor(getOutlineColor());
        g.drawRect(boundingBox.x, boundingBox.y, boundingBox.width, boundingBox.height);

        // Draw the center of mass
        super.drawBoundingBox(g, referenceHeight);
    }

    private Rectangle getTranslatedBoundingBox(int referenceHeight) {
        int topLeftX = (int) (getCenterPt().getX() - radius);
        int topLeftY = referenceHeight - (int) (getCenterPt().getY() + radius);
        return new Rectangle(topLeftX, topLeftY, (int) getDiameter(), (int) getDiameter());
    }

    private double getDiameter() {
        return radius * 2;
    }

}