package main.java.simulator;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;

import static main.java.simulator.utils.Key.EXIT;
import static main.java.simulator.utils.Key.PAUSE;

public class SimulatorPanel extends JPanel implements Runnable, KeyListener {

    private static final int WIDTH = 320;
    private static final int HEIGHT = 240;
    private static final int SCALE = 2;

    private Thread thread;
    private boolean running = true;
    private boolean pause = true;

    private BufferedImage image;
    private Graphics2D graphics2D;

    private World world;

    public SimulatorPanel() {
        super();
        setPreferredSize(new Dimension(WIDTH * SCALE, HEIGHT * SCALE));
        setFocusable(true);
        requestFocus();
        init();
    }

    public World getWorld() {
        return world;
    }

    @Override
    public void addNotify() {
        super.addNotify();
        if (thread == null) {
            addKeyListener(this);
            thread = new Thread(this);
            thread.start();
        }
    }

    private void init() {
        image = new BufferedImage(WIDTH * SCALE, HEIGHT * SCALE, BufferedImage.TYPE_INT_RGB);
        graphics2D = (Graphics2D) image.getGraphics();
        running = true;
        world = new World();
    }

    private void update(double dt) {
        world.update(dt);
    }

    private void render() {
        world.render(graphics2D);
        if (isPaused()) {
            graphics2D.setFont(new Font("TimesRoman", Font.PLAIN, 20));
            graphics2D.drawString("PAUSE", (getWidthScaled() - graphics2D.getFont().getSize()) / 2, getHeightScaled() / 2);
        }
    }

    private void draw() {
        Graphics g2 = getGraphics();
        g2.drawImage(image, 0, 0, null);
        g2.dispose();
    }

    @Override
    public void run() {

        final double TARGET_UPDATES = 60;
        final double MS_PER_SECOND = 1000.0;
        final double TIME_BETWEEN_UPDATES = MS_PER_SECOND / TARGET_UPDATES;

        double previous = System.currentTimeMillis();
        double timer = previous;

        //int ups = 0;
        while (running) {
            if (!isPaused()) {
                double current = System.currentTimeMillis();
                double elapsed = current - previous;

                update(elapsed / MS_PER_SECOND);
                //ups++;
                render();
                draw();
                if (System.currentTimeMillis() - timer > MS_PER_SECOND) {
                    //System.out.println("UPS: " + ups);
                    //ups = 0;
                    timer = System.currentTimeMillis();
                }

                final long sleepTime = (long) (current + TIME_BETWEEN_UPDATES - System.currentTimeMillis());

                try {
                    if (sleepTime > 0) {
                        Thread.sleep(sleepTime);
                    }
                } catch (InterruptedException e) {
                    System.out.println(e);
                }
                previous = current;
                continue; // skip the next sentence
            }
            render();
            draw();
            previous = System.currentTimeMillis();
        }
    }

    private boolean isPaused() {
        return pause;
    }

    private void setPause(boolean pause) {
        this.pause = pause;
    }

    public static int getWidthScaled() {
        return WIDTH * SCALE;
    }

    public static int getHeightScaled() {
        return HEIGHT * SCALE;
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        System.out.println(e.getKeyCode());
        if (e.getKeyCode() == PAUSE.getKeyCode()) {
            setPause(!isPaused());
        }
        if (e.getKeyCode() == EXIT.getKeyCode()) {
            System.exit(0);
        }
        world.keyPressed(e);
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }
}
