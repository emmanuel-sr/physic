package main.java.simulator;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.*;

public class Simulator {

    public static void main(String[] args) {
        JFrame window = new JFrame("Simulator");
        JPanel wrapper = new JPanel();
        SimulatorPanel sp = new SimulatorPanel();
        wrapper.setLayout(new BoxLayout(wrapper, BoxLayout.Y_AXIS));
        
        InputPanel menu = new InputPanel(sp.getWorld().getBodies());
        
        menu.setLayout(new BoxLayout(menu, BoxLayout.Y_AXIS));

        wrapper.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseReleased(e);
                sp.requestFocus();
            }
        });

        wrapper.add(sp);
        wrapper.add(menu);
        window.setContentPane(wrapper);
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.setResizable(false);
        window.pack();
        window.setVisible(true);
    }
}
