package main.java.simulator.utils;

public enum Key {

    PAUSE(32), EXIT(27), RESTART(82), GRAVITY(71);

    private final int keyCode;

    Key(int keyCode) {
        this.keyCode = keyCode;
    }

    public int getKeyCode() {
        return this.keyCode;
    }
}
