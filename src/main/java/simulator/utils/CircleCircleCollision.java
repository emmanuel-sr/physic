package main.java.simulator.utils;

import main.java.simulator.model.Circle;
import main.java.simulator.model.Vector;

public class CircleCircleCollision {

    private static Vector getTranslationVectors(Vector mtd, Circle mainCircle, Circle otherCircle) {
        // Checking if the velocity of main polygon is 0
        if (mainCircle.getVelocity().getX() == 0 && mainCircle.getVelocity().getY() == 0) {
            return new Vector(0, 0);
        }

        // Making sure the push vector is pushing the polygons away
        Vector translationVector = mtd.clone();

        Vector displacementBetweenPolygons = mainCircle.getCenterPt().subtract(otherCircle.getCenterPt());
        if (displacementBetweenPolygons.dotProduct(mtd) < 0) {
            translationVector.multiplyBy(-1);
        }

        // Get the ratio of the translation vector when both objects are moving
        double curLength = translationVector.getLength();
        double lengthOfMainVelocity = mainCircle.getVelocity().getLength();
        double lengthOfOtherVelocity = otherCircle.getVelocity().getLength();
        double newLength = curLength * (lengthOfMainVelocity / (lengthOfMainVelocity + lengthOfOtherVelocity));

        translationVector.setLength(newLength);

        // Checking if the new translation vector is a null (happens if the length of translation vector is a 0)
        if (Double.isNaN(translationVector.getX()) && Double.isNaN(translationVector.getY())) {
            translationVector.setY(0);
            translationVector.setX(0);
        }

        return translationVector;
    }

    public static boolean doBodiesCollide(Circle circle1, Circle circle2,
                                          Vector circle1TransVector, Vector circle2TransVector, Vector mtd) {
        // Compute the distance between the two circles as well as the sum of their radiuses
        double centerPtDistance = circle1.getCenterPt().distance(circle2.getCenterPt());
        double radiusSum = circle1.getRadius() + circle2.getRadius();

        // If there is a collision
        if (centerPtDistance < radiusSum) {

            // Calculate the MTD
            Vector mtdVector = circle1.getCenterPt().subtract(circle2.getCenterPt());
            mtdVector.setLength(radiusSum - centerPtDistance);

            // Get the translation vector
            Vector circle1Trans = getTranslationVectors(mtdVector, circle1, circle2);
            Vector circle2Trans = getTranslationVectors(mtdVector, circle2, circle1);

            // Save the trans vectors to the variables in the parameters
            circle1TransVector.setX(circle1Trans.getX());
            circle1TransVector.setY(circle1Trans.getY());

            circle2TransVector.setX(circle2Trans.getX());
            circle2TransVector.setY(circle2Trans.getY());

            mtd.setX(mtdVector.getX());
            mtd.setY(mtdVector.getY());
            return true;
        }
        return false;
    }
}
